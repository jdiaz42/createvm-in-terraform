# configure azure provider
provider "azurerm" {
  subscription_id = "e0324dcd-bb83-44a6-80b3-343353697b79"
  client_id       = "f1724e36-9d1f-4a0a-bad5-8c9f4bf34561"
  client_secret   = "Db1hvg5E95CQ-azDSCISpi@S:dfkqup-"
  tenant_id       = "93f33571-550f-43cf-b09f-cd331338d086"
}

resource "azurerm_resource_group" "rg1" {
  name     = "acctestrg"
  location = "West US"
}

resource "azurerm_network_security_group" "nsg" {
    name                = "mynsg"
    location            = "westus"
    resource_group_name = "${azurerm_resource_group.rg1.name}"

    security_rule {
        name                       = "SSH"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }

    tags = {
        environment = "Terraform Demo"
    }
}


# Create LB
resource "azurerm_lb" "lb" {
  resource_group_name = "${azurerm_resource_group.rg1.name}"
  name                = "lb"
  location            = "West US"

  frontend_ip_configuration {
    name                 = "LoadBalancerFrontEnd"
    public_ip_address_id = "${azurerm_public_ip.pub2.id}"
  }
}

resource "azurerm_lb_backend_address_pool" "backend_pool" {
  resource_group_name = "${azurerm_resource_group.rg1.name}"
  loadbalancer_id     = "${azurerm_lb.lb.id}"
  name                = "BackendPool1"
}

resource "azurerm_lb_nat_rule" "tcp" {
  resource_group_name            = "${azurerm_resource_group.rg1.name}"
  loadbalancer_id                = "${azurerm_lb.lb.id}"
  name                           = "SSH"
  protocol                       = "tcp"
  frontend_port                  = "22"
  backend_port                   = 22
  frontend_ip_configuration_name = "LoadBalancerFrontEnd"
  count                          = 2
}

resource "azurerm_lb_rule" "lb_rule" {
  resource_group_name            = "${azurerm_resource_group.rg1.name}"
  loadbalancer_id                = "${azurerm_lb.lb.id}"
  name                           = "LBRule"
  protocol                       = "tcp"
  frontend_port                  = 80
  backend_port                   = 80
  frontend_ip_configuration_name = "LoadBalancerFrontEnd"
  enable_floating_ip             = false
  backend_address_pool_id        = "${azurerm_lb_backend_address_pool.backend_pool.id}"
  idle_timeout_in_minutes        = 5
  probe_id                       = "${azurerm_lb_probe.lb_probe.id}"
  depends_on                     = ["azurerm_lb_probe.lb_probe"]
}

resource "azurerm_lb_probe" "lb_probe" {
  resource_group_name = "${azurerm_resource_group.rg1.name}"
  loadbalancer_id     = "${azurerm_lb.lb.id}"
  name                = "tcpProbe"
  protocol            = "tcp"
  port                = 80
  interval_in_seconds = 5
  number_of_probes    = 2
}

# Create network Interface 1
resource "azurerm_network_interface" "ni1" {
  name                = "ni1"
  location            = "West US"
  resource_group_name = "${azurerm_resource_group.rg1.name}"
  network_security_group_id = "${azurerm_network_security_group.nsg.id}"
  ip_configuration {
    name                          = "config1"
    subnet_id                     = "${azurerm_subnet.sub1.id}"
    private_ip_address_allocation = "dynamic"
    public_ip_address_id  = "${azurerm_public_ip.pub1.id}"

  }
}

# Create network Interface 1
resource "azurerm_network_interface" "ni2" {
  name                = "ni2"
  location            = "West US"
  resource_group_name = "${azurerm_resource_group.rg1.name}"
  network_security_group_id = "${azurerm_network_security_group.nsg.id}"
  ip_configuration {
    name                          = "config2"
    subnet_id                     = "${azurerm_subnet.sub1.id}"
    private_ip_address_allocation = "dynamic"
    public_ip_address_id  = "${azurerm_public_ip.pub3.id}"

  }
}

# Create Vnet1
resource "azurerm_virtual_network" "vn1" {
  name                = "vn1"
  address_space       = ["10.0.0.0/16"]
  location            = "West US"
  resource_group_name = "${azurerm_resource_group.rg1.name}"
}



# Create public ip 1
resource "azurerm_public_ip" "pub1" {
  name                         = "pub1"
  location                     = "West US"
  resource_group_name          = "${azurerm_resource_group.rg1.name}"
  allocation_method = "Dynamic"
}

# Create public ip 1
resource "azurerm_public_ip" "pub2" {
  name                         = "pub2"
  location                     = "West US"
  resource_group_name          = "${azurerm_resource_group.rg1.name}"
  allocation_method = "Dynamic"
}

# Create public ip 3
resource "azurerm_public_ip" "pub3" {
  name                         = "pub3"
  location                     = "West US"
  resource_group_name          = "${azurerm_resource_group.rg1.name}"
  allocation_method = "Dynamic"
}

# Create subnet 1
resource "azurerm_subnet" "sub1" {
  name                 = "sub1"
  resource_group_name  = "${azurerm_resource_group.rg1.name}"
  virtual_network_name = "${azurerm_virtual_network.vn1.name}"
  address_prefix       = "10.0.1.0/24"
}

# Create subnet 1
resource "azurerm_subnet" "sub2" {
  name                 = "sub2"
  resource_group_name  = "${azurerm_resource_group.rg1.name}"
  virtual_network_name = "${azurerm_virtual_network.vn1.name}"
  address_prefix       = "10.0.2.0/24"
}

# Create subnet 1
resource "azurerm_subnet" "sub3" {
  name                 = "sub3"
  resource_group_name  = "${azurerm_resource_group.rg1.name}"
  virtual_network_name = "${azurerm_virtual_network.vn1.name}"
  address_prefix       = "10.0.3.0/24"
}
 resource "azurerm_storage_account" "storevm123" {
  name                = "storevm123"
  resource_group_name = "${azurerm_resource_group.rg1.name}"
  location            = "westus"
  account_tier        = "Standard"
  account_replication_type        = "LRS"
}

# Create VM 1
 resource "azurerm_virtual_machine" "vm1" {
  name                  = "vm1"
  location              = "West US"
  resource_group_name   = "${azurerm_resource_group.rg1.name}"
  network_interface_ids = ["${azurerm_network_interface.ni1.id}"]
  vm_size               = "Standard_B1s"
  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04.0-LTS"
    version   = "latest"
  }


# Create OS Disk
  storage_os_disk {
    name              = "myosdisk1"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
  os_profile {
        computer_name  = "ubuntu"
        admin_username = "azureuser1"
        custom_data = "${file("/home/azure/cloud-init.txt")}"
    }

  os_profile_linux_config {
        disable_password_authentication = true
        ssh_keys {
            path     = "/home/azureuser1/.ssh/authorized_keys"
            key_data = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC4G6GwMKrToNwnAmSdvA14gEH3LaAnK4WYk3QmdFV2+LMFfG/b/oLIfoaOFP+yx5hocPxxaZ3eyTcT57ViOaCXi/cbNk5qerbKubkHGJCev7p+n+D56RR/uRhTo7ODzihMm9dQJhhPfgpmzLG8hThfG21Y3QBPY2LqWzgRrbec+mOYLzB0t34PU88lDRi254W2hT7LI+C7efetX3JlvlYfO+LhEqmfutL1THAkSjff301Uun7KFiE5pwm/BAjv9RCNAzcHJ4yx7ia282LYPWpYGy5PFKYZpr8LgUxVwcOjVqcOq+AI8aiOe/qc08LXgOhBzjQu8+P7NowRe89wVLhz root@ansible"

        }
    }
  }

# Create VM 2
 resource "azurerm_virtual_machine" "vm2" {
  name                  = "vm2"
  location              = "West US"
  resource_group_name   = "${azurerm_resource_group.rg1.name}"
  network_interface_ids = ["${azurerm_network_interface.ni2.id}"]
  vm_size               = "Standard_B1s"
  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04.0-LTS"
    version   = "latest"
  }


# Create OS Disk
  storage_os_disk {
    name              = "myosdisk2"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
  os_profile {
        computer_name  = "ubuntu"
        admin_username = "azureuser2"
        custom_data = "${file("/home/azure/cloud-init.txt")}"
    }

  os_profile_linux_config {
        disable_password_authentication = true
        ssh_keys {
            path     = "/home/azureuser2/.ssh/authorized_keys"
            key_data = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC4G6GwMKrToNwnAmSdvA14gEH3LaAnK4WYk3QmdFV2+LMFfG/b/oLIfoaOFP+yx5hocPxxaZ3eyTcT57ViOaCXi/cbNk5qerbKubkHGJCev7p+n+D56RR/uRhTo7ODzihMm9dQJhhPfgpmzLG8hThfG21Y3QBPY2LqWzgRrbec+mOYLzB0t34PU88lDRi254W2hT7LI+C7efetX3JlvlYfO+LhEqmfutL1THAkSjff301Uun7KFiE5pwm/BAjv9RCNAzcHJ4yx7ia282LYPWpYGy5PFKYZpr8LgUxVwcOjVqcOq+AI8aiOe/qc08LXgOhBzjQu8+P7NowRe89wVLhz root@ansible"
            }

    }

 }



   #storage_data_disk {
   # name          = "datadisk1"
   # vhd_uri       = "${azurerm_storage_account.storevm123.primary_blob_endpoint}${azurerm_storage_container.cont1.name}/datadisk1.vhd"
   # disk_size_gb  = "60"
   # create_option = "Empty"
   # lun           = 0
  #}
